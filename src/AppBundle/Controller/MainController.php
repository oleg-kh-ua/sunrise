<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
class MainController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        //Получаем всю недвижимость из базы данных
        $houses = $this->get('doctrine')->getRepository('AppBundle:House', 'default')->findAll();

        //Отрисовуем главную страницу с параметрами из базы данных
        return $this->render(
            'main/main.html.twig',
            array('houses' => $houses)
        );
    }

    /**
     * @Route("/sort/{city}/{type}/{from}/{to}")
     */
    //http://localhost:8000/sort/Kharkiv/sale/10000/135000
    public function sortAction($city, $type, $from, $to)
    {
        $price = 'price';
        $area = 'area';

        $from = $from * 0.9;
        $to = $to * 1.1;

        if($type == 'sale'){
            $forType = $price;
        }else{
            $forType = $area;
        }


        $query = "Select * FROM home WHERE city='".$city."' AND " . $forType . " BETWEEN '".$from."' AND '".$to."'";
        $productsByCategory = $this->get('doctrine')->getManager();
        $conn = $productsByCategory->getConnection();
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $houses = $stmt->fetchAll();

        return new JsonResponse($houses);
    }


}